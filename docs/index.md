# 이미지 처리를 위한 컨벌루션 신경망 <sup>[1](#footnote_1)</sup>

> <font size="3">이미지 처리 작업에 컨볼루션 신경망을 사용하는 방법에 대해 알아본다.</font>

## 목차

1. [개요](./convolutional-neural-networks.md#intro)
1. [컨볼루션 신경망이란?](./convolutional-neural-networks.md#sec_02)
1. [컨볼루션 신경망은 어떻게 작동할까?](./convolutional-neural-networks.md#sec_03)
1. [영상처리를 위한 컨볼루션 신경망의 응용](./convolutional-neural-networks.md#sec_04)
1. [영상 처리를 위한 컨볼루션 신경망 구축과 훈련 방법](./convolutional-neural-networks.md#sec_05)
1. [영상처리를 위한 컨볼루션 신경망의 과제와 한계](./convolutional-neural-networks.md#sec_06)
1. [마치며](./convolutional-neural-networks.md#summary)

<a name="footnote_1">1</a>: [ML Tutorial 9 — Convolutional Neural Networks for Image Processing](https://ai.gopubby.com/ml-tutorial-9-convolutional-neural-networks-for-image-processing-28a206154aef?sk=dec9508d81e1604420c060058257c7f5)를 편역하였다.
